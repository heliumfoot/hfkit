//  HFAVCameraCaptureManager.m
//	HFKit
//
//  Created by Keith Alperin on 04/02/12.
//  Copyright (c) 2012 Helium Foot Software. All rights reserved.

/*
     File: AVCamCaptureManager.m
 Abstract: Code that calls the AVCapture classes to implement the camera-specific features in the app such as recording, still image, camera exposure, white balance and so on.
  Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 */

/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "HFAVCameraCaptureManager.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>


@interface HFAVCameraCaptureManager (AVCaptureFileOutputRecordingDelegate) <AVCaptureFileOutputRecordingDelegate>
@end

@interface HFAVCameraCaptureManager (AVCaptureVideoDataOutputSampleBufferDelegate) <AVCaptureVideoDataOutputSampleBufferDelegate>
-(CGFloat) radiansForDegrees:(CGFloat) degrees;
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer; 

@end

@interface HFAVCameraCaptureManager ()

@property (nonatomic,strong) AVCaptureSession *session;
@property (nonatomic,strong) AVCaptureDeviceInput *videoInput;
@property (nonatomic,strong) AVCaptureDeviceInput *audioInput;
@property (nonatomic,strong) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic,strong) AVCaptureStillImageOutput *stillImageOutput;
@property(nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic,strong) id deviceConnectedObserver;
@property (nonatomic,strong) id deviceDisconnectedObserver;
@property (nonatomic,assign) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, assign, getter = isCancelingRecording) BOOL cancelingRecording; 
@property (nonatomic, assign, getter = isCapturingNextVideoFrame) BOOL capturingNextVideoFrame; 
@property(nonatomic, strong)NSURL *currentlyRecordingVideoURL;
@property(nonatomic, weak)id<HFCameraCoreImageFrameCapturingDelegate> coreImageFrameCapturingDelegate;
@property(nonatomic, weak)id<HFCameraUIImageFrameCapturingDelegate> uiImageFrameCapturingDelegate;
@end

@interface HFAVCameraCaptureManager (Internal)

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition)position;
- (AVCaptureDevice *) frontFacingCamera;
- (AVCaptureDevice *) backFacingCamera;
- (AVCaptureDevice *) audioDevice;
- (NSURL *) tempFileURL;
-(AVCaptureVideoOrientation)captureVideoOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation;

@end

const NSTimeInterval kFlashDuration = 0.001;
NSString * const kNotifcationFocusAdjusted = @"kNotifcationFocusAdjusted";
NSString * const kAdjustingFocusKeyPath = @"adjustingFocus";

@implementation HFAVCameraCaptureManager

@synthesize session;
@synthesize orientation;
@dynamic audioChannel;
@dynamic sessionPreset;
@synthesize mirroringMode;
@synthesize videoInput;
@synthesize audioInput;
@dynamic flashMode;
@dynamic torchMode;
@dynamic focusMode;
@dynamic exposureMode;
@dynamic whiteBalanceMode;
@synthesize movieFileOutput;
@synthesize stillImageOutput;
@synthesize videoDataOutput;
@synthesize deviceConnectedObserver;
@synthesize deviceDisconnectedObserver;
@synthesize backgroundRecordingID;
@synthesize delegate;
@dynamic recording;
@synthesize cancelingRecording;
@synthesize capturingNextVideoFrame;
@synthesize captureMode;
@synthesize currentlyRecordingVideoURL;
@synthesize currentCameraDevice;
@synthesize coreImageFrameCapturingDelegate;
@synthesize uiImageFrameCapturingDelegate;

- (id) init
{
    self = [super init];
    if (self != nil) {
		
        void (^deviceConnectedBlock)(NSNotification *) = ^(NSNotification *notification) {
            
            [self.session beginConfiguration];
            [self.session removeInput:[self audioInput]];

			AVCaptureDeviceInput *newAudioInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self audioDevice] error:nil];
			if ([self.session canAddInput:newAudioInput]) {                
				[self.session addInput:newAudioInput];
			}

            AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self backFacingCamera] error:nil];
            [self.session removeInput:[self videoInput]];
            if ([self.session canAddInput:newVideoInput]) {
                [self.session addInput:newVideoInput];
            }
            [self.session commitConfiguration];
            
			
            [self setAudioInput:newAudioInput];
            [self setVideoInput:newVideoInput];
            
            if ([self.delegate respondsToSelector:@selector(deviceCountChanged)]) {
                [self.delegate deviceCountChanged];
            }
            
            if (![self.session isRunning]) {
                [self.session startRunning];
			}
        };
        void (^deviceDisconnectedBlock)(NSNotification *) = ^(NSNotification *notification) {
            
            [self.session beginConfiguration];
            
            if (![[[self audioInput] device] isConnected]) {
                [self.session removeInput:[self audioInput]];
			}
            if (![[[self videoInput] device] isConnected]) {
                [self.session removeInput:[self videoInput]];
			}
                
            [self.session commitConfiguration];
            
            [self setAudioInput:nil];
            
            if ([self.delegate respondsToSelector:@selector(deviceCountChanged)]) {
                [self.delegate deviceCountChanged];
            }
            
            if (![self.session isRunning]) {
                [self.session startRunning];
			}
        };
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [self setDeviceConnectedObserver:[notificationCenter addObserverForName:AVCaptureDeviceWasConnectedNotification object:nil queue:nil usingBlock:deviceConnectedBlock]];
        [self setDeviceDisconnectedObserver:[notificationCenter addObserverForName:AVCaptureDeviceWasDisconnectedNotification object:nil queue:nil usingBlock:deviceDisconnectedBlock]];            
    }
    return self;
}


- (void) dealloc
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:[self deviceConnectedObserver]];
    [notificationCenter removeObserver:[self deviceDisconnectedObserver]];
    [self.currentCameraDevice removeObserver:self forKeyPath:kAdjustingFocusKeyPath];

    [[self session] stopRunning];
}

- (BOOL) setupSessionWithMode:(HFCameraCaptureMode)aCaptureMode preset:(NSString *)sessionPreset preferredCamera:(AVCaptureDevicePosition)preferredCamera error:(NSError **)error;
{
    BOOL success = NO;
    
//	//enable audio playback and recording at the same time
//	AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//	NSError *audioError = nil;
//	BOOL succeeded = [audioSession setCategory:AVAudioSessionCategoryAmbient error:&audioError];
//	if (!succeeded) {
//		if (audioError != nil) {
//			NSLog(@"Unresolved error setting audio session category %@, %@", audioError, [audioError userInfo]);
//		}
//	}
//
//	//with thanks to unsynchronized: http://stackoverflow.com/a/7426406/16572
//	UInt32 doSetProperty = 1;
//    AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof(doSetProperty), &doSetProperty);
//	audioError = nil;
//	succeeded = [audioSession setActive:YES error: &audioError];
//	if (!succeeded) {
//		if (audioError != nil) {
//			NSLog(@"Unresolved error activating audio session %@, %@", audioError, [audioError userInfo]);
//		}
//	}

	
	// Init the device inputs
	
	//do we have the preferred camera?
	AVCaptureDevice *cameraDevice = [self cameraWithPosition:preferredCamera];
	if (cameraDevice == nil) {
		cameraDevice = self.backFacingCamera;
		
		if (cameraDevice == nil) {
			NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
			if ([devices count] > 0) {
				cameraDevice = [devices objectAtIndex:0];
			}
		}
	}
	
    AVCaptureDeviceInput *aVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:cameraDevice error:error];
    [self setVideoInput:aVideoInput];
    
    AVCaptureDeviceInput *anAudioInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self audioDevice] error:error];
    [self setAudioInput:anAudioInput];
    
    // Setup the default file outputs
    AVCaptureStillImageOutput *aStillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    AVVideoCodecJPEG, AVVideoCodecKey,
                                    nil];
    [aStillImageOutput setOutputSettings:outputSettings];
    [self setStillImageOutput:aStillImageOutput];
    
	AVCaptureMovieFileOutput *aMovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    [self setMovieFileOutput:aMovieFileOutput];
    

	
    // Add inputs and output to the capture session, set the preset, and start it running
    self.session = [[AVCaptureSession alloc] init];
    
    if ([self.session canAddInput:aVideoInput]) {
        [self.session addInput:aVideoInput];
    }
    if ([self.session canAddInput:anAudioInput]) {
        [self.session addInput:anAudioInput];
    }

    if ([self.session canAddOutput:aMovieFileOutput]) {
        [self.session addOutput:aMovieFileOutput];
        [self setMirroringMode:HFCameraMirroringAuto];
    }

    if ([self.session canAddOutput:aStillImageOutput]) {
        [self.session addOutput:aStillImageOutput];
    }
	
	[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification 
													  object:self.session 
													   queue:[NSOperationQueue mainQueue] 
												  usingBlock:^(NSNotification *notification) {
													  NSDictionary *userInfo = [notification userInfo];
													  NSError *error = [userInfo objectForKey: AVCaptureSessionErrorKey];
													  if (error != nil) {
														  NSLog(@"Unresolved AVCaptureSessionRuntimeErrorNotification %@, %@", error, [error userInfo]);
													  }
													  
												  }
	 ];
	
	
	//set the capture mode
	//self.captureMode = aCaptureMode;
	
	
    [self setSessionPreset:sessionPreset];
    
	//listen for focus adjustments
	//[self.currentCameraDevice addObserver:self forKeyPath:kAdjustingFocusKeyPath options:NSKeyValueObservingOptionNew context:nil];
	
	//XXX keith says: this looks like it was a bug in the apple sample code where the session was set after the pre-set, but it could be a bug in how well keith understands this sample code.
    //[self setSession:aSession];
    
	
    
    success = YES;
    
    if ([self.delegate respondsToSelector:@selector(deviceCountChanged)]) {
        [self.delegate deviceCountChanged];
    }
    
    return success;
}

-(void)setCaptureMode:(HFCameraCaptureMode)aCaptureMode
{
	if (self.captureMode != aCaptureMode) {
		captureMode = aCaptureMode;
		if (self.captureMode == HFCameraCaptureModeStill) {
			//remove the video file capture from the session
			[self.session removeOutput:self.movieFileOutput];
			self.movieFileOutput = nil;
			
			//enable the reading of frames from the video camera by adding a AVCaptureVideoDataOutput to the session
			self.videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
			if ([self.session canAddOutput:self.videoDataOutput]) {
				[self.session addOutput:self.videoDataOutput];
				
				// Configure your output.
				dispatch_queue_t queue = dispatch_queue_create("myQueue", NULL);
				[self.videoDataOutput setSampleBufferDelegate:self queue:queue];
				
				// Specify the pixel format
				self.videoDataOutput.videoSettings = 
				[NSDictionary dictionaryWithObject:
				 [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA] 
											forKey:(id)kCVPixelBufferPixelFormatTypeKey];
				
				AVCaptureConnection *videoConnection = [HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self videoDataOutput] connections]];
				if ([videoConnection isVideoOrientationSupported]) {
					[videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
				}
				
			}
			
		} else {
//			//remove the AVCaptureVideoDataOutput from the session
//			[self.session removeOutput:self.videoDataOutput];
//			self.videoDataOutput = nil;
//			
//			//add back the video file capture
//			self.movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
//			[self setMovieFileOutput:self.movieFileOutput];
//			
//			if ([self.session canAddOutput:self.movieFileOutput]) {
//				[self.session addOutput:self.movieFileOutput];
//				[self setMirroringMode:HFCameraMirroringAuto];
//			}
		}		
	}
}


- (void) stopSession
{
	[self.session stopRunning];
}
- (void) startSession
{
	[self.session startRunning];
	self.cancelingRecording = NO;
}
- (BOOL) isSessionRunning
{
	return self.session.running;
}

- (BOOL) isRecording
{
    return [[self movieFileOutput] isRecording];
}

- (void) startRecording
{
    if ([[UIDevice currentDevice] isMultitaskingSupported]) {
        [self setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{}]];
    }
    
    AVCaptureConnection *videoConnection = [HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self movieFileOutput] connections]];
    if ([videoConnection isVideoOrientationSupported]) {
        [videoConnection setVideoOrientation:[self orientation]];
    }
    
	self.currentlyRecordingVideoURL = [self tempFileURL];
    [[self movieFileOutput] startRecordingToOutputFileURL:self.currentlyRecordingVideoURL
                                        recordingDelegate:self];
}

- (void) stopRecording
{
    [[self movieFileOutput] stopRecording];
	self.currentlyRecordingVideoURL = nil;
	
}

- (void) cancelRecording
{
	self.cancelingRecording = YES;
	[self stopRecording];
	
}
- (void) captureStillImage
{
    AVCaptureConnection *videoConnection = [HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self stillImageOutput] connections]];
    if ([videoConnection isVideoOrientationSupported]) {
        [videoConnection setVideoOrientation:[self orientation]];
    }
   	
    [[self stillImageOutput] 
		captureStillImageAsynchronouslyFromConnection:videoConnection
									completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
										if (imageDataSampleBuffer != NULL) {
											NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
											UIImage *image = [[UIImage alloc] initWithData:imageData];                                                                 
											//get the meta data
											CFDictionaryRef metadataDict = CMCopyDictionaryOfAttachments(NULL, imageDataSampleBuffer, kCMAttachmentMode_ShouldPropagate);
											NSDictionary *metaDataDictionary = (__bridge NSMutableDictionary *)metadataDict;
											
											if ([self.delegate respondsToSelector:@selector(captureStillImageSucceededWithImage:metadata:)]) {
												[self.delegate captureStillImageSucceededWithImage:image metadata:metaDataDictionary];
											}
											CFRelease(metadataDict);
										} else if (error) {
											if ([self.delegate respondsToSelector:@selector(captureStillImageFailedWithError:)]) {
												[self.delegate captureStillImageFailedWithError:error];
											}
										} else {
											NSLog(@"couldn't take a picture");
										}
									}];
}

- (void) captureNextVideoUIImageFrameWithDelegate:(id<HFCameraUIImageFrameCapturingDelegate>)aFrameCapturingDelegate
{
	self.uiImageFrameCapturingDelegate = aFrameCapturingDelegate;
	if (self.captureMode == HFCameraCaptureModeStill) {
		
		//set a flag saying that we need the next frame;
		self.capturingNextVideoFrame = YES;
	} else {
		//take a still picture
		[self captureStillImage];
		
	}
	
}

- (void) captureNextVideoCoreImageFrameWithDelegate:(id<HFCameraCoreImageFrameCapturingDelegate>)aFrameCapturingDelegate
{
	self.coreImageFrameCapturingDelegate = aFrameCapturingDelegate;
	if (self.captureMode == HFCameraCaptureModeStill) {

		//set a flag saying that we need the next frame;
		self.capturingNextVideoFrame = YES;
	} else {
		//take a still picture
		[self captureStillImage];
 
	}
		
}

- (BOOL) cameraToggle
{
    BOOL success = NO;
    [self.currentCameraDevice removeObserver:self forKeyPath:kAdjustingFocusKeyPath];
    if ([self cameraCount] > 1) {
        NSError *error;
        AVCaptureDeviceInput *newVideoInput;
        AVCaptureDevicePosition position = [[self.videoInput device] position];
        BOOL mirror;
        if (position == AVCaptureDevicePositionBack) {
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self frontFacingCamera] error:&error];
            switch ([self mirroringMode]) {
                case HFCameraMirroringOff:
                    mirror = NO;
                    break;
                case HFCameraMirroringOn:
                    mirror = YES;
                    break;
                case HFCameraMirroringAuto:
                default:
                    mirror = NO;
                    break;
            }
        } else if (position == AVCaptureDevicePositionFront) {
            newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self backFacingCamera] error:&error];
            switch ([self mirroringMode]) {
                case HFCameraMirroringOff:
                    mirror = NO;
                    break;
                case HFCameraMirroringOn:
                    mirror = YES;
                    break;
                case HFCameraMirroringAuto:
                default:
                    mirror = YES;
                    break;
            }
        } else {
            goto bail;
        }
        
        if (newVideoInput != nil) {
            [self.session beginConfiguration];
            [self.session removeInput:videoInput];
            NSString *currentPreset = [self.session sessionPreset];
            if (![[newVideoInput device] supportsAVCaptureSessionPreset:currentPreset]) {
                [self.session setSessionPreset:AVCaptureSessionPresetHigh]; // default back to high, since this will always work regardless of the camera
            }
            if ([self.session canAddInput:newVideoInput]) {
                [self.session addInput:newVideoInput];
                AVCaptureConnection *connection = [HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self movieFileOutput] connections]];
                if ([connection isVideoMirroringSupported]) {
                    [connection setVideoMirrored:mirror];
                }
                [self setVideoInput:newVideoInput];
            } else {
                [self.session setSessionPreset:currentPreset];
                [self.session addInput:videoInput];
            }
            [self.session commitConfiguration];
            success = YES;
        } else if (error) {
            if ([self.delegate respondsToSelector:@selector(someOtherError:)]) {
                [self.delegate someOtherError:error];
            }
        }
    }
	[self.currentCameraDevice addObserver:self forKeyPath:kAdjustingFocusKeyPath options:NSKeyValueObservingOptionNew context:nil];

bail:
    return success;
}

- (NSUInteger) cameraCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

- (NSUInteger) micCount
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] count];
}

- (BOOL) hasFlash
{
    return [[[self videoInput] device] hasFlash];
}

- (AVCaptureFlashMode) flashMode
{
    return [[[self videoInput] device] flashMode];
}

- (void) setFlashMode:(AVCaptureFlashMode)flashMode
{
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isFlashModeSupported:flashMode] && [device flashMode] != flashMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }    
    }
}

-(NSArray *)flashModes
{
	static NSArray *flashModes = nil;
	if (flashModes == nil) {
		flashModes = [[NSArray alloc] initWithObjects:
					  [NSNumber numberWithInteger:AVCaptureFlashModeAuto]
					  ,[NSNumber numberWithInteger:AVCaptureFlashModeOn]
					  ,[NSNumber numberWithInteger:AVCaptureFlashModeOff]
					  , nil];
	}
	
	return flashModes;
}					  
					  
- (BOOL) hasTorch
{
    return [[[self videoInput] device] hasTorch];
}

- (AVCaptureTorchMode) torchMode
{
    return [[[self videoInput] device] torchMode];
}

- (void) setTorchMode:(AVCaptureTorchMode)torchMode
{
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isTorchModeSupported:torchMode] && [device torchMode] != torchMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setTorchMode:torchMode];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }
    }
}



-(void)flashNumberOfTimes:(NSInteger)count
{
	for (NSInteger i=0;i<count;i++) {
		[self performSelector:@selector(singleFlash) withObject:nil afterDelay:(kFlashDuration * 4) * i];
	}
}

-(void) singleFlash
{
	if ([self hasTorch]) {
		self.torchMode = AVCaptureTorchModeOn;
		[self performSelector:@selector(torchOff) withObject:nil afterDelay:kFlashDuration];
	}
}
-(void)torchOff
{
	self.torchMode = AVCaptureTorchModeOff;
}

- (BOOL) hasFocus
{
    AVCaptureDevice *device = [[self videoInput] device];
    
    return  [device isFocusModeSupported:AVCaptureFocusModeLocked] ||
            [device isFocusModeSupported:AVCaptureFocusModeAutoFocus] ||
            [device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus];
}

- (AVCaptureFocusMode) focusMode
{
    return [[[self videoInput] device] focusMode];
}

- (void) setFocusMode:(AVCaptureFocusMode)focusMode
{
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isFocusModeSupported:focusMode] && [device focusMode] != focusMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setFocusMode:focusMode];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }    
    }
}

- (BOOL) hasExposure
{
    AVCaptureDevice *device = [[self videoInput] device];
    
    return  [device isExposureModeSupported:AVCaptureExposureModeLocked] ||
            [device isExposureModeSupported:AVCaptureExposureModeAutoExpose] ||
            [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure];
}

- (AVCaptureExposureMode) exposureMode
{
    return [[[self videoInput] device] exposureMode];
}

- (void) setExposureMode:(AVCaptureExposureMode)exposureMode
{
    if (exposureMode == 1) {
        exposureMode = 2;
    }
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isExposureModeSupported:exposureMode] && [device exposureMode] != exposureMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setExposureMode:exposureMode];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }
    }
}

- (BOOL) hasWhiteBalance
{
    AVCaptureDevice *device = [[self videoInput] device];
    
    return  [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeLocked] ||
            [device isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeAutoWhiteBalance];
}

- (AVCaptureWhiteBalanceMode) whiteBalanceMode
{
    return [[[self videoInput] device] whiteBalanceMode];
}

- (void) setWhiteBalanceMode:(AVCaptureWhiteBalanceMode)whiteBalanceMode
{
    if (whiteBalanceMode == 1) {
        whiteBalanceMode = 2;
    }    
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isWhiteBalanceModeSupported:whiteBalanceMode] && [device whiteBalanceMode] != whiteBalanceMode) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setWhiteBalanceMode:whiteBalanceMode];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }
    }
}

- (void) focusAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setFocusPointOfInterest:point];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }        
    }
}

- (void) exposureAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = [[self videoInput] device];
    if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setExposurePointOfInterest:point];
            [device setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
            [device unlockForConfiguration];
        } else {
            if ([self.delegate respondsToSelector:@selector(acquiringDeviceLockFailedWithError:)]) {
                [self.delegate acquiringDeviceLockFailedWithError:error];
            }
        }
    }    
}

- (NSString *) sessionPreset
{
    return [[self session] sessionPreset];
}

- (void) setSessionPreset:(NSString *)aSessionPreset
{
    if (![aSessionPreset isEqualToString:[self sessionPreset]] && [session canSetSessionPreset:aSessionPreset]) {
        [self.session beginConfiguration];
        [self.session setSessionPreset:aSessionPreset];
        [self.session commitConfiguration];
    }
}

- (void) setConnectionWithMediaType:(NSString *)mediaType enabled:(BOOL)enabled;
{
    [[HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self movieFileOutput] connections]] setEnabled:enabled];
}

- (void) setMirroringMode:(HFCameraMirroringMode)aMirroringMode
{
    aMirroringMode = mirroringMode;
    AVCaptureConnection *fileConnection = [HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self movieFileOutput] connections]];
    AVCaptureConnection *stillConnection = [HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self stillImageOutput] connections]];
    [self.session beginConfiguration];
    switch (mirroringMode) {
        case HFCameraMirroringOff:
            if ([fileConnection isVideoMirroringSupported]) {
                [fileConnection setVideoMirrored:NO];
            }
            if ([stillConnection isVideoMirroringSupported]) {
                [stillConnection setVideoMirrored:NO];
            }
            break;
        case HFCameraMirroringOn:
            if ([fileConnection isVideoMirroringSupported]) {
                [fileConnection setVideoMirrored:YES];
            }
            if ([stillConnection isVideoMirroringSupported]) {
                [stillConnection setVideoMirrored:YES];
            }
            break;
        case HFCameraMirroringAuto:
        {
            BOOL mirror = NO;
            AVCaptureDevicePosition position = [[[self videoInput] device] position];
            if (position == AVCaptureDevicePositionBack) {
                mirror = NO;
            } else if (position == AVCaptureDevicePositionFront) {
                mirror = YES;
            }
            if ([fileConnection isVideoMirroringSupported]) {
                [fileConnection setVideoMirrored:mirror];
            }
            if ([stillConnection isVideoMirroringSupported]) {
                [stillConnection setVideoMirrored:mirror];
            }
        }
            break;
    }
    [self.session commitConfiguration];
}

- (BOOL) supportsMirroring
{
    return [[HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self movieFileOutput] connections]] isVideoMirroringSupported] ||
            [[HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeVideo fromConnections:[[self stillImageOutput] connections]] isVideoMirroringSupported];
}


- (AVCaptureAudioChannel *)audioChannel
{
    return [[[HFAVCameraCaptureManager connectionWithMediaType:AVMediaTypeAudio fromConnections:[[self movieFileOutput] connections]] audioChannels] lastObject];
}

+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections;
{
	for ( AVCaptureConnection *connection in connections ) {
		for ( AVCaptureInputPort *port in [connection inputPorts] ) {
			if ( [[port mediaType] isEqual:mediaType] ) {
				return connection;
			}
		}
	}
	return nil;
}

@end

@implementation HFAVCameraCaptureManager (Internal)

- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}

- (AVCaptureDevice *) frontFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionFront];
}

- (AVCaptureDevice *) backFacingCamera
{
    return [self cameraWithPosition:AVCaptureDevicePositionBack];
}

- (AVCaptureDevice *) currentCameraDevice
{
    return self.videoInput.device;
}

	 
- (AVCaptureDevice *) audioDevice
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    if ([devices count] > 0) {
        return [devices objectAtIndex:0];
    }
    return nil;
}

- (NSURL *) tempFileURL
{
    NSString *outputPath = [[NSString alloc] initWithFormat:@"%@%1.0f.mov", NSTemporaryDirectory(), [[NSDate date] timeIntervalSinceReferenceDate]];
    NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:outputPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:outputPath]) {
        NSError *error;
        if ([fileManager removeItemAtPath:outputPath error:&error] == NO) {
            if ([self.delegate respondsToSelector:@selector(someOtherError:)]) {
                [self.delegate someOtherError:error];
            }            
        }
    }
    return outputURL;
}

-(AVCaptureVideoOrientation)orientation
{
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
	AVCaptureVideoOrientation currentOrientation = [self captureVideoOrientationForDeviceOrientation:deviceOrientation];
	return currentOrientation;
}

-(AVCaptureVideoOrientation)captureVideoOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
	AVCaptureVideoOrientation captureOrienation;
	
	switch (deviceOrientation) {
		case UIDeviceOrientationPortrait:
			captureOrienation = AVCaptureVideoOrientationPortrait;
			break;
		case UIDeviceOrientationPortraitUpsideDown:
			captureOrienation = AVCaptureVideoOrientationPortraitUpsideDown;
			break;
		case UIDeviceOrientationLandscapeLeft:
			captureOrienation = AVCaptureVideoOrientationLandscapeRight;
			break;
		case UIDeviceOrientationLandscapeRight:
			captureOrienation = AVCaptureVideoOrientationLandscapeLeft;
			break;
		default:
			captureOrienation = AVCaptureVideoOrientationLandscapeRight;
			break;
	}
	
	return captureOrienation;
	
}

@end



@implementation HFAVCameraCaptureManager (AVCaptureFileOutputRecordingDelegate)

- (void)             captureOutput:(AVCaptureFileOutput *)captureOutput
didStartRecordingToOutputFileAtURL:(NSURL *)fileURL
                   fromConnections:(NSArray *)connections
{
    if ([self.delegate respondsToSelector:@selector(recordingBeganToOutputFileAtURL:)]) {
        [self.delegate recordingBeganToOutputFileAtURL:fileURL];
    }
}

- (void)              captureOutput:(AVCaptureFileOutput *)captureOutput
didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
                    fromConnections:(NSArray *)connections
                              error:(NSError *)error
{
    if (error && [self.delegate respondsToSelector:@selector(someOtherError:)]) {
        [self.delegate someOtherError:error];
    }
    
    if ([[UIDevice currentDevice] isMultitaskingSupported]) {
        [[UIApplication sharedApplication] endBackgroundTask:[self backgroundRecordingID]];
    }
    
    if (!self.cancelingRecording && [self.delegate respondsToSelector:@selector(recordingFinishedToOutputFileAtURL:)]) {
        [self.delegate recordingFinishedToOutputFileAtURL:outputFileURL];
    }
	self.cancelingRecording = NO;
}

@end

@implementation HFAVCameraCaptureManager (AVCaptureVideoDataOutputSampleBufferDelegate)
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
	if (self.capturingNextVideoFrame) {
		self.capturingNextVideoFrame = NO;
		
		if (self.uiImageFrameCapturingDelegate != nil) {
			[self.uiImageFrameCapturingDelegate captureFrameSucceededWithUIImage:[self imageFromSampleBuffer:sampleBuffer] options:nil];
			self.uiImageFrameCapturingDelegate = nil;
		}

		if (self.coreImageFrameCapturingDelegate) {
			CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
			CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
			CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(__bridge NSDictionary *)attachments];
			CGSize targetSize = CGSizeMake(320.0, 426.0);
			CGSize imageSize = ciImage.extent.size;
			CFRelease(attachments);
			CGAffineTransform rotateTransform = CGAffineTransformIdentity;
			CGFloat scaleFactor = MAX(targetSize.width / imageSize.width, targetSize.height / imageSize.height);
			CGAffineTransform scaleTransform = CGAffineTransformMakeScale(scaleFactor, scaleFactor);

			UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
			
			AVCaptureDevicePosition position = [[self.videoInput device] position];
			if (deviceOrientation == UIDeviceOrientationPortrait && position == AVCaptureDevicePositionFront) {
				rotateTransform = CGAffineTransformMakeRotation([self radiansForDegrees:180]);
			} else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown && position == AVCaptureDevicePositionBack) {
				rotateTransform = CGAffineTransformMakeRotation([self radiansForDegrees:180]);
			} else if (deviceOrientation == UIDeviceOrientationLandscapeLeft) { 
				rotateTransform = CGAffineTransformMakeRotation([self radiansForDegrees:90]);
			} else if (deviceOrientation == UIDeviceOrientationLandscapeRight) { 
				rotateTransform = CGAffineTransformMakeRotation([self radiansForDegrees:-90]);
			}

			CGAffineTransform transform;
			if (!CGAffineTransformEqualToTransform(rotateTransform, CGAffineTransformIdentity)) {
				transform = CGAffineTransformConcat(scaleTransform, rotateTransform);
			} else {
				transform = scaleTransform;
			}
			
			ciImage = [ciImage imageByApplyingTransform:transform]; 
			UIImage *uiImage = [UIImage imageWithCIImage:ciImage];
			ciImage = [uiImage CIImage];
			NSDictionary *imageOptions = nil;
			NSNumber *cgImageOrientation = (__bridge id)CMGetAttachment(sampleBuffer, kCGImagePropertyOrientation, NULL);
			if (cgImageOrientation != nil) {
				imageOptions = [NSDictionary dictionaryWithObject:cgImageOrientation forKey:CIDetectorImageOrientation];
			}
			[self.coreImageFrameCapturingDelegate captureFrameSucceededWithImage:ciImage options:imageOptions];
				
		}
		


	}
}

//from QA1702
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer 
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer); 
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0); 
	
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer); 
	
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer); 
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer); 
    size_t height = CVPixelBufferGetHeight(imageBuffer); 
	
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB(); 
	
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, 
												 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst); 
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context); 
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
	
    // Free up the context and color space
    CGContextRelease(context); 
    CGColorSpaceRelease(colorSpace);
	
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
	
    // Release the Quartz image
    CGImageRelease(quartzImage);
	
    return (image);
}

-(CGFloat) radiansForDegrees:(CGFloat) degrees
{
    return M_PI * (degrees / 180.0);
}
#pragma mark KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:kAdjustingFocusKeyPath]) {
		if ([change objectForKey:NSKeyValueChangeNewKey] != nil && [[change objectForKey:NSKeyValueChangeNewKey] boolValue] == NO) {
			NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
			[notificationCenter postNotificationName:kNotifcationFocusAdjusted object:self userInfo:nil];
		}

	}
}
@end

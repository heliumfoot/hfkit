//
//  HFVerticallyAlignedLabel.m
//	HFKit
//
//  Created by keith Alperin on 7/27/12.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "HFVerticallyAlignedLabel.h"

@interface HFVerticallyAlignedLabel ()
@end

@implementation HFVerticallyAlignedLabel

@synthesize verticalAlignment;
-(void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)setVerticalAlignment:(HFVerticalAlignment)aVerticalAlignment
{
	verticalAlignment = aVerticalAlignment;
	[self setNeedsDisplay];
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
	CGRect textRect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
	switch (self.verticalAlignment) {
		case HFVerticalAlignmentTop:
			textRect.origin.y = bounds.origin.y;
			if (textRect.size.height < bounds.size.height) {
				textRect.origin.y -= 2.0;//keith says: a little bit of fudging
			}

			break;
		case HFVerticalAlignmentBottom:
			textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
			break;
		case HFVerticalAlignmentMiddle:
			// Fall through.
		default:
			textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
	}
	
	
	return textRect;
}

-(void)drawTextInRect:(CGRect)requestedRect {
	CGRect actualRect = [self textRectForBounds:requestedRect limitedToNumberOfLines:self.numberOfLines];
	[super drawTextInRect:actualRect];
}

@end
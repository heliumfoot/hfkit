//
//  HFBaseEntityBuilder.h
//	HFKit
//
//  Created by keith Alperin on 7/26/13.
//  Copyright (c) 2013 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import <Foundation/Foundation.h>

@interface HFBaseEntityBuilder : NSObject
-(id)initWithBackgroundManagedObjectContext:(NSManagedObjectContext*)backgroundManagedObjectContext entityIDExternalKeyName:(NSString*)entityIDExternalKeyName entityIDInternalKeyName:(NSString*)entityIDInternalKeyName;
-(id)buildOrUpdateEntityWithResultsDictionary:(NSDictionary*)resultDictionary;

//protected
@property(nonatomic,strong, readonly)NSManagedObjectContext* backgroundManagedObjectContext;
-(Class)entityType;
-(void)updateEntity:(id)entity withResultDictionary:(NSDictionary*)dictionary;
-(id)entityForID:(NSInteger)entityID ofType:(Class)type inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext;
-(NSInteger)entityIDForResultDictionary:(NSDictionary*)dictionary;

@end

//
//  UIViewController+HFExtensions.m
//	HFKit
//
//  Created by keith Alperin on 12/25/12.
//  Copyright (c) 2012 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "UIViewController+HFExtensions.h"

@interface UIViewController (HFExtensionsPrivate)
-(NSArray *)hfCurrentViewControllersForViewController:(UIViewController *)rootViewController;
@end

@implementation UIViewController (HFExtensions)
-(IBAction)hfDismiss:(id)sender
{	
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)hfShuttleObjectsWithNames:(NSArray *)names toDestinationViewController:(id)destinationViewController
{
	for (NSString *name in names) {
		@try {
			[destinationViewController setValue:[self valueForKey:name] forKey:name];
		}
		@catch (NSException *exception) {
		}
		@finally {
			
		}
	}
}

#pragma mark View Controller Traversal
-(id)hfCurrentViewController
{
	NSArray *viewControllerHierarchy = [self hfCurrentViewControllersForViewController:self];
	UIViewController *current = [viewControllerHierarchy lastObject];
	return current;
	
}
-(NSArray *)hfCurrentViewControllersForViewController:(UIViewController *)rootViewController
{
	NSMutableArray *someViewControllers = [NSMutableArray array];
	UIViewController *current = rootViewController;
	if ([rootViewController isKindOfClass:[UINavigationController class]]) {
		current = [(UINavigationController *)rootViewController visibleViewController];
	} else if ([rootViewController isKindOfClass:[UITabBarController class]]) {
		UITabBarController *tabBarController = (UITabBarController *)rootViewController;
		current = tabBarController.selectedViewController;
		if (current == nil) {
			current = [tabBarController.viewControllers objectAtIndex:0];
		}
	} else if ([rootViewController respondsToSelector:@selector(selectedViewController)]) {
		current = [(id)rootViewController selectedViewController];
	} else if (rootViewController.presentedViewController != nil) {
		current = rootViewController.presentedViewController;
	}
	
	if (current != nil) {
		[someViewControllers addObject:current];
		if (![rootViewController isEqual:current]) {
			//recurse in case we have nested view controller containers
			[someViewControllers addObjectsFromArray:[self hfCurrentViewControllersForViewController:current]];
		}
	}
	
	return someViewControllers;
	
}

-(id)hfCurrentViewControllerOfType:(Class)type
{
	UIViewController *current = nil;
	
	if ([self isKindOfClass:type]) {
		current = self;
	} else {
		NSArray *viewControllerHierarchy = [self hfCurrentViewControllersForViewController:self];
		
		for (UIViewController *viewController in viewControllerHierarchy) {
			if ([viewController isKindOfClass:type]) {
				current = viewController;
				break;
			}
		}
	}
	
	
	return current;
}

-(id)hfRootViewControllerOfType:(Class)type
{
	
	UIViewController *current = nil;
	NSArray *viewControllerHierarchy = [self hfCurrentViewControllersForViewController:self];
	
	//keith says, if the current view controller is of the correct type, then we insert it at the beginning since we're going to reverse the array to find the root
	if ([self isKindOfClass:type]) {
		NSMutableArray *tempArray = [NSMutableArray arrayWithArray:viewControllerHierarchy];
		[tempArray insertObject:self atIndex:0];
		viewControllerHierarchy = [NSArray arrayWithArray:tempArray];
	}

	//keith says: we reverse the array because we want to find the view controller LOWEST in the hierarchy that matches our type
	for (UIViewController *viewController in [viewControllerHierarchy reverseObjectEnumerator]) {
		if ([viewController isKindOfClass:type]) {
			current = viewController;
			break;
		}
	}
	
	return current;

	
}

@end

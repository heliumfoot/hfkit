//
//  UIViewController+HFKeyboardHandlingExtensions.m
//
//  Created by keith Alperin on 11/14/13.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/



#import "UIViewController+HFKeyboardHandlingExtensions.h"
#import <objc/runtime.h>

@interface UIViewController (HFKeyboardHandlingExtensionsPrivate)
@property (nonatomic) NSString *insetsNameKey;
-(UIScrollView*)hfScrollViewForHFViewController;
@end


@implementation UIViewController (HFKeyboardHandlingExtensions)
-(void)hfRegisterForKeyboardNotifications
{
	NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
	[notificationCenter addObserver:self selector:@selector(hfKeyboardDidAppear:) name:UIKeyboardWillShowNotification object:nil];
	[notificationCenter addObserver:self selector:@selector(hfKeyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];

}

-(void)hfDeregisterForKeyboardNotifications
{
	NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
	[notificationCenter removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[notificationCenter removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)hfKeyboardDidAppear:(NSNotification *)notification
{
	NSDictionary *userInfo = notification.userInfo;
	CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
	
    keyboardFrame = [self.view convertRect:keyboardFrame fromView:nil];
    
    CGFloat keyboardHeight = keyboardFrame.size.height;
	
	UIScrollView* scrollView = self.hfScrollViewForHFViewController;
	
	if (scrollView != nil) {
		UIEdgeInsets originalInsets = scrollView.contentInset;
		NSValue* originalInsetsValue = [NSValue valueWithUIEdgeInsets:originalInsets];
			
		objc_setAssociatedObject(self, @selector(insetsNameKey), originalInsetsValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
		
		UIEdgeInsets contentInsets = UIEdgeInsetsMake(originalInsets.top, 0.0, keyboardHeight, 0.0);
		
		
		scrollView.contentInset = contentInsets;
		scrollView.scrollIndicatorInsets = contentInsets;
	}
}

-(void)hfKeyboardDidHide:(NSNotification *)notification
{
	NSValue* originalInsetsValue = objc_getAssociatedObject(self, @selector(insetsNameKey));
	UIEdgeInsets contentInsets = [originalInsetsValue UIEdgeInsetsValue];
	UIScrollView* scrollView = self.hfScrollViewForHFViewController;
	
	scrollView.contentInset = contentInsets;
	scrollView.scrollIndicatorInsets = contentInsets;
}


-(UIScrollView*)hfScrollViewForHFViewController
{

	UIScrollView* scrollView = nil;
	
	id possibleScrollView = [self valueForKey:@"tableView"];
	if ([possibleScrollView isKindOfClass:[UIScrollView class]]) {
		scrollView = possibleScrollView;
	} else {
		possibleScrollView = [self valueForKey:@"scrollView"];
		if ([possibleScrollView isKindOfClass:[UIScrollView class]]) {
			scrollView = possibleScrollView;
		}
	}

	return possibleScrollView;

	
}
@end

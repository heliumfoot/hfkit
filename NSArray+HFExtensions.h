//
//  NSArray+HFExtensions.h
//	HFKit
//
//  Created by keith Alperin on 5/21/12.
//  Copyright (c) 2012 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import <Foundation/Foundation.h>

@interface NSArray (HFExtensions)
-(NSArray *)hfArrayByMapingWithBlock: (id (^)(id obj))block;
/**
 returns a new NSArray starting at location range.location with a length up to range.length but will not fail if there are fewer than range.length items remaining in the array.  
 If range.location is greater than self.count then an empty array is returned
 */
-(NSArray *)hfSubarrayUpToRange:(NSRange)range;
@end

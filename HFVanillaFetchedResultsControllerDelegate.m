//
//  VanillaFetchedResultsControllerDelegate.m
//
//  Created by keith Alperin on 7/25/13.
//  Copyright (c) 2013 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
	(i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
	(ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
	(iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/


#import "HFVanillaFetchedResultsControllerDelegate.h"
#import "NSIndexSet+HFExtensions.h"


@interface HFVanillaFetchedResultsControllerDelegate ()
@property(nonatomic,strong)UITableView* tableView;
@property (nonatomic, copy) void (^insertCellBlock)(NSIndexPath* indexPath);
@property (nonatomic, copy) void (^deleteCellBlock)(NSIndexPath* indexPath);
@property (nonatomic, copy) void (^moveCellBlock)(NSIndexPath* deletedRowIndexPath,NSIndexPath* insertedRowIndexPath);
@property (nonatomic, copy) void (^endUpdatesBlock)();
@property (nonatomic) BOOL ignoringUpdates;

@property (nonatomic, strong)NSMutableIndexSet *deletedSectionIndexes;
@property (nonatomic, strong)NSMutableIndexSet *insertedSectionIndexes;
@property(nonatomic, strong)NSMutableArray* insertedRowIndexPaths;
@property(nonatomic, strong)NSMutableArray* movedItems;
@property(nonatomic, strong)NSMutableArray* deletedRowIndexPaths;

@end

@interface MovingItemInfo : NSObject

@property(nonatomic, strong)NSIndexPath* deletingIndexPath;
@property(nonatomic, strong)NSIndexPath* insertingIndexPath;

-(instancetype)initWithIndexPath:(NSIndexPath*)indexPath insertingIndexPath:(NSIndexPath*)insertingIndexPath;

@end


@implementation HFVanillaFetchedResultsControllerDelegate
- (id)initWithTableView:(UITableView*)tableView insertCellBlock:(void (^)(NSIndexPath* indexPath))insertCellBlock deleteCellBlock:(void (^)(NSIndexPath* indexPath))deleteCellBlock
{
	self = [self initWithTableView:tableView
				   insertCellBlock:insertCellBlock
				   deleteCellBlock:deleteCellBlock
					 moveCellBlock:nil
				   endUpdatesBlock:nil
			];
	if (self) {
	}
	return self;
}

- (id)initWithTableView:(UITableView*)tableView insertCellBlock:(void (^)(NSIndexPath* indexPath))insertCellBlock deleteCellBlock:(void (^)(NSIndexPath* indexPath))deleteCellBlock moveCellBlock:(void (^)(NSIndexPath* deletedRowIndexPath,NSIndexPath* insertedRowIndexPath))moveCellBlock endUpdatesBlock:(void (^)())endUpdatesBlock;
{
    self = [super init];
    if (self) {
        self.tableView = tableView;
		self.insertCellBlock = insertCellBlock;
		self.deleteCellBlock = deleteCellBlock;
		self.moveCellBlock = moveCellBlock;
		self.endUpdatesBlock = endUpdatesBlock;
		//self.insertingIndexPaths = [NSMutableSet set];
		
		self.insertedSectionIndexes = [NSMutableIndexSet indexSet];
		self.deletedSectionIndexes =  [NSMutableIndexSet indexSet];
		self.insertedRowIndexPaths = [NSMutableArray array];
		self.movedItems = [NSMutableArray array];
		self.deletedRowIndexPaths = [NSMutableArray array];
    }
    return self;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	[self.insertedSectionIndexes removeAllIndexes];
	[self.deletedSectionIndexes removeAllIndexes];
	
	[self.insertedRowIndexPaths removeAllObjects];
	[self.movedItems removeAllObjects];
	[self.deletedRowIndexPaths removeAllObjects];
    if (!self.ignoringUpdates) {
		[CATransaction begin];
		if (self.tableAnimationCompletionBlock != nil) {
			__weak typeof(self) weakSelf = self;
			[CATransaction setCompletionBlock:^{
				weakSelf.tableAnimationCompletionBlock([NSSet setWithArray:weakSelf.insertedRowIndexPaths]);
			}];
		}
		[self.tableView beginUpdates];
	}
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
		   atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	
	if (!self.ignoringUpdates) {

		switch(type) {
			case NSFetchedResultsChangeInsert:
				
				[self.insertedSectionIndexes addIndex:sectionIndex];
				break;
				
			case NSFetchedResultsChangeDelete:
				[self.deletedSectionIndexes addIndex:sectionIndex];
				break;
			case NSFetchedResultsChangeUpdate:
			case NSFetchedResultsChangeMove:
			default:
				break;
		}
	}
}

-(void)ignoreUpdates
{
	self.ignoringUpdates = YES;
	
}

-(void)resumeUpdates
{
	self.ignoringUpdates = NO;

}
	

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
	   atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
	  newIndexPath:(NSIndexPath *)newIndexPath {
	if (!self.ignoringUpdates) {
		UITableView *tableView = self.tableView;
		
		MovingItemInfo* movingItemInfo = [[MovingItemInfo alloc] initWithIndexPath:indexPath insertingIndexPath:newIndexPath];

		switch(type) {
				
			case NSFetchedResultsChangeInsert:
				[self.insertedRowIndexPaths addObject:newIndexPath];
				
				break;
				
			case NSFetchedResultsChangeDelete:
				[self.deletedRowIndexPaths addObject:indexPath];
				break;
				
			case NSFetchedResultsChangeUpdate:
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                break;
				
			case NSFetchedResultsChangeMove:
				[self.movedItems addObject:movingItemInfo];
				
				break;
		}
	}
	
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	if (!self.ignoringUpdates){
		//first handle the sections
		[self.tableView deleteSections:self.deletedSectionIndexes withRowAnimation:UITableViewRowAnimationFade];
		[self.tableView insertSections:self.insertedSectionIndexes withRowAnimation:UITableViewRowAnimationFade];
		
		
		//next, filter deletedRowIndexPaths to take out anything from the inserted section
		NSPredicate* notDeletedSectionPredicate = [NSPredicate predicateWithFormat:@"NOT (section IN %@)", [self.deletedSectionIndexes hfAsNumberSet]];
		NSArray* availableDeletedRowIndexPaths = [self.deletedRowIndexPaths filteredArrayUsingPredicate:notDeletedSectionPredicate];
		
		//call the callbacks on ALL of the deleted index paths
		if (self.deleteCellBlock != nil) {
			for (NSIndexPath* deletedIndexPath in self.deletedRowIndexPaths) {
				self.deleteCellBlock(deletedIndexPath);
			}
		}
		
		//now add all of the deletedIndexPaths from the movingItems that are not in an delted section
		NSArray* deletedWhileMovingIndexPaths = [self.movedItems valueForKey:@"deletingIndexPath"];
		NSArray* availableDeletedWhilemovingRowIndexPaths = [deletedWhileMovingIndexPaths filteredArrayUsingPredicate:notDeletedSectionPredicate];
		availableDeletedRowIndexPaths = [availableDeletedRowIndexPaths arrayByAddingObjectsFromArray:availableDeletedWhilemovingRowIndexPaths];
		

		//delete the AVAILABLE rows
		[self.tableView deleteRowsAtIndexPaths:availableDeletedRowIndexPaths
							  withRowAnimation:UITableViewRowAnimationFade];
		
		//same for Inserted
		NSPredicate* notInsertedSectionPredicate = [NSPredicate predicateWithFormat:@"NOT (section IN %@)", [self.insertedSectionIndexes hfAsNumberSet]];
		NSArray* availableInsertedRowIndexPaths = [self.insertedRowIndexPaths filteredArrayUsingPredicate:notInsertedSectionPredicate];

		//call the callbacks on ALL of the inserted index paths
		if (self.insertCellBlock != nil) {
			for (NSIndexPath* insertingIndexPath in self.insertedRowIndexPaths) {
				self.insertCellBlock(insertingIndexPath);
			}
		}
		
		//now add all of the insertedIndexPaths from the movingItems that are not in an delted section
		NSArray* insertedWhileMovingIndexPaths = [self.movedItems valueForKey:@"insertingIndexPath"];
		NSArray* availableInsertedWhilemovingRowIndexPaths = [insertedWhileMovingIndexPaths filteredArrayUsingPredicate:notInsertedSectionPredicate];
		availableInsertedRowIndexPaths = [availableInsertedRowIndexPaths arrayByAddingObjectsFromArray:availableInsertedWhilemovingRowIndexPaths];

		//insert the AVAILABLE rows
		[self.tableView insertRowsAtIndexPaths:availableInsertedRowIndexPaths
							  withRowAnimation:UITableViewRowAnimationFade];

		
		
		//call the callbacks on ALL of the moving
		if (self.moveCellBlock != nil) {
			for (MovingItemInfo* movingItem in self.movedItems) {
				self.moveCellBlock(movingItem.deletingIndexPath, movingItem.insertingIndexPath);
			}
		}
		
		
		
		[self.tableView endUpdates];
		[CATransaction commit];

		if (self.endUpdatesBlock != nil) {
			self.endUpdatesBlock();
		}
	}
	
}

@end

@implementation MovingItemInfo

-(instancetype)initWithIndexPath:(NSIndexPath*)indexPath insertingIndexPath:(NSIndexPath*)insertingIndexPath
{
	self = [super init];
	if (self) {
		self.deletingIndexPath = indexPath;
		self.insertingIndexPath = insertingIndexPath;
	}
	return self;
}

@end

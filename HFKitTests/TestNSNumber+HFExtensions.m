//
//  TestNSNumber+HFExtensions.m
//  HydroXphere
//
//  Created by keith alperin on 1/29/16.
//  Copyright © 2016 HydroXphere LLC. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSNumber+HFExtensions.h"
@interface TestNSNumber_HFExtensions : XCTestCase
-(NSInteger)numberOfDecimalDigitsInString:(NSString*)string;

@end

@implementation TestNSNumber_HFExtensions

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testStringValueRoundedToPlaces {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
	
	NSString* zeroDigits = [@1 hfStringValueRoundedToPlaces:0];
	XCTAssertEqual([self numberOfDecimalDigitsInString:zeroDigits], 0);

	NSNumber* templateNumber = @(1.234564789);
	NSString* alsoZeroDigits = [templateNumber hfStringValueRoundedToPlaces:0];
	XCTAssertEqual([self numberOfDecimalDigitsInString:alsoZeroDigits], 0);
	
	NSString* oneDigits = [templateNumber hfStringValueRoundedToPlaces:1];
	XCTAssertEqual([self numberOfDecimalDigitsInString:oneDigits], 1);
	
	NSString* twoDigits = [templateNumber hfStringValueRoundedToPlaces:2];
	XCTAssertEqual([self numberOfDecimalDigitsInString:twoDigits], 2);

	float twoDigitsAsFloat = [twoDigits floatValue];
	XCTAssertFalse([@(twoDigitsAsFloat) hfIsEffectivelyEqualToNumber:templateNumber]);
	XCTAssertTrue([@(twoDigitsAsFloat) hfIsEffectivelyEqualToNumber:@(1.23)]);
	
	
}

-(NSInteger)numberOfDecimalDigitsInString:(NSString*)string
{
	NSInteger numberOfDigits = 0;
	NSArray* components = [string componentsSeparatedByString:@"."];
	if (components.count == 2) {
		numberOfDigits = [[components lastObject] length];
	}
	return numberOfDigits;
}


@end

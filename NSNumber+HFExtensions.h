//
//  NSNumber+HFExtensions.h
//  HFKit
//
//  Copyright (c) 2014 Helium Foot Software All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
	(i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
	(ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
	(iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import <Foundation/Foundation.h>

@interface NSNumber (HFExtensions)
/**
 *	Useful in determining if two floating point numbers are effectively equal.
 *
 *	@param otherNumber
 *
 *	@return YES if the two numbers are very close together.
 */
-(BOOL)hfIsEffectivelyEqualToNumber:(NSNumber*)otherNumber;

/**
 *	<#Description#>
 *
 *	@param decimalPlaces	<#decimalPlaces description#>
 *
 *	@return rounds self to the number of decimal places and returns it as a string.  Does not include trailing zeros
 */
-(NSString*)hfStringValueRoundedToPlaces:(short)decimalPlaces;
@end

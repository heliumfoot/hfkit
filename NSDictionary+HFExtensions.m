//
//  NSDictionary+HFExtensions.m
//	HFKit
//
//  Created by Keith Alperin on 10/30/13.
//  Copyright 2013 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "NSDictionary+HFExtensions.h"
@interface NSDictionary (HFExtensionsPrivate)
-(id)hfSafeValueForValue:(id)value;
@end


@implementation NSDictionary (HFExtensions)
-(id)hfSafeObjectForKey:(id<NSCopying>)key
{
	id value = [self objectForKey:key];
	value = [self hfSafeValueForValue:value];
	return value;
}
-(id)hfSafeValueForKeyPath:(NSString *)keyPath
{
	id value = [self valueForKeyPath:
				keyPath];
	value = [self hfSafeValueForValue:value];
	return value;
	
}

-(id)hfSafeValueForValue:(id)value
{
	if (value == [NSNull null]) {
		value = nil;
	}
	return value;
	
}
@end

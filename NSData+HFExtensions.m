//
//  NSData+HFExtensions.m
//	HFKit
//
//  Created by Keith Alperin on 4/30/12.
//  Copyright 2010 Helium Foot Software. All rights reserved.
//

/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import <CommonCrypto/CommonCrypto.h>

#import "NSData+HFExtensions.h"

@implementation NSData (HFExtensions)
/**
 with thanks to Dave Gallagher: http://stackoverflow.com/a/9084784/16572
 */
- (NSString *)hfHexadecimalString
{
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
	
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
	
    if (!dataBuffer) {
        return [NSString string];
	}
	
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
	
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
	}
	
    return [NSString stringWithString:hexString];
}

/**
 With thanks to Saurabh Sharma: http://www.makebetterthings.com/iphone/how-to-get-md5-and-sha1-in-objective-c-ios-sdk/
 */
-(NSString*) hfSha1Digest
{
	
	uint8_t digest[CC_SHA1_DIGEST_LENGTH];
	
	CC_SHA1(self.bytes, (CC_LONG)self.length, digest);
	
	NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
	
	for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
		[output appendFormat:@"%02x", digest[i]];
	}
	
	return output;
	
}

-(NSString *)hfStringByMD5Hashing
{
	const char *cStr = self.bytes;
	unsigned char result[CC_MD5_DIGEST_LENGTH];
	CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]
			];
	

}

@end

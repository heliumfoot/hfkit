//
//  HFKit.h
//  HFKit
//
//  Created by keith Alperin on 10/15/14.
//  Copyright (c) 2014 Helium Foot Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

//! Project version number for HFKit.
FOUNDATION_EXPORT double HFKitVersionNumber;

//! Project version string for HFKit.
FOUNDATION_EXPORT const unsigned char HFKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HFKit/PublicHeader.h>


#import <HFKit/HFAVCameraCaptureManager.h>
#import <HFKit/HFBaseEntityBuilder.h>
#import <HFKit/HFCoreDataHelper.h>
#import <HFKit/HFFrameHelper.h>
#import <HFKit/HFGradientView.h>
#import <HFKit/HFManagedObjectContextHelper.h>
#import <HFKit/HFManagedObjectContextHelperForTests.h>
#import <HFKit/HFTestWindow.h>
#import <HFKit/HFVanillaFetchedResultsControllerDelegate.h>
#import <HFKit/HFVerticallyAlignedLabel.h>
#import <HFKit/NSArray+HFExtensions.h>
#import <HFKit/NSAttributedString+HFExtensions.h>
#import <HFKit/NSData+HFExtensions.h>
#import <HFKit/NSDictionary+HFExtensions.h>
#import <HFKit/NSFetchRequest+HFExtensions.h>
#import <HFKit/NSString+HFExtensions.h>
#import <HFKit/NSString+HFPaths.h>
#import <HFKit/UIColor+HFExtensions.h>
#import <HFKit/UIImage+HFExtensions.h>
#import <HFKit/UIScrollView+HFExtensions.h>
#import <HFKit/UITableView+HFExtensions.h>
#import <HFKit/UIView+HFExtensions.h>
#import <HFKit/UIViewController+HFExtensions.h>
#import <HFKit/UIViewController+HFKeyboardHandlingExtensions.h>

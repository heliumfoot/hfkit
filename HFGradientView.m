//
//  HFGradientView.m
//	HFKit
//
//  Created by keith Alperin on 7/23/12.
//  Copyright (c) 2012 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import <QuartzCore/QuartzCore.h>

#import "HFGradientView.h"
#import "NSArray+HFExtensions.h"

@interface HFGradientView ()
@property(nonatomic, strong, readonly)CAGradientLayer *gradientLayer;
@property(nonatomic, assign)CGFloat pulseCount;
@property(nonatomic, strong)NSArray *alternateColors;
-(NSArray *)cgColorsForUIColors:(NSArray *)uiColors;
-(NSArray *)uiColorsForCGColors:(NSArray *)cgColors;
-(void)updateColors;
@end

@implementation HFGradientView
+(Class)layerClass
{
	return [CAGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.pulseCount = FLT_MAX;
    }
    return self;
}

-(NSArray *)cgColorsForUIColors:(NSArray *)uiColors
{
	NSArray *cgColors = [uiColors hfArrayByMapingWithBlock:^id(id obj) {
		if ([obj isKindOfClass:[UIColor class]]) {
			return (id)[(UIColor *)obj CGColor];
		} else {
			return obj;
		}
	}];
	return cgColors;
}

-(NSArray *)uiColorsForCGColors:(NSArray *)cgColors;
{
	NSArray *uiColors = [self.gradientLayer.colors hfArrayByMapingWithBlock:^id(id obj) {
		if ([obj isKindOfClass:[UIColor class]]) {
			return obj;
		} else {
			return [UIColor colorWithCGColor:(CGColorRef)obj];
		}
	}];
	return uiColors;
}

-(void)pulseWithAlternateColors:(NSArray *)alternateColors numberOfTimes:(CGFloat)pulseCount
{
	
	self.pulseCount = pulseCount==0?FLT_MAX:pulseCount;
	self.alternateColors = alternateColors;
}
-(void)stopPulsing
{
	[self pulseWithAlternateColors:nil numberOfTimes:FLT_MAX];
}
-(void)updateColors
{
	NSString *animationKey = @"animateGradient";
	if (self.colors != nil && self.alternateColors != nil) {
		[self.layer removeAnimationForKey:animationKey];
		NSTimeInterval delayInSeconds = 0.01;
		dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
		dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
			CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"colors"];
			animation.fromValue = self.colors;
			animation.toValue = self.alternateColors;
			animation.duration	= 1.0;
			animation.removedOnCompletion = NO;
			animation.fillMode = kCAFillModeForwards;
			animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
			animation.autoreverses = YES;
			animation.repeatCount = self.pulseCount;
			[self.layer addAnimation:animation forKey:animationKey];
		});
	} else {
		[self.layer removeAnimationForKey:animationKey];
	}
}

-(void)setColors:(NSArray *)colors
{
	self.gradientLayer.colors = [self cgColorsForUIColors:colors];
	[self updateColors];
}
-(NSArray *)colors
{
	return self.gradientLayer.colors;
}
-(void)setAlternateColors:(NSArray *)colors
{
	__alternateColors = [self cgColorsForUIColors:colors];
	[self updateColors];
}
-(NSArray *)alternateColors
{
	return __alternateColors;
}


-(CGPoint)startPoint
{
	return self.gradientLayer.startPoint;
}
-(void)setStartPoint:(CGPoint)startPoint
{
	self.gradientLayer.startPoint = startPoint;
}

-(CGPoint)endPoint
{
	return self.gradientLayer.endPoint;
}
-(void)setEndPoint:(CGPoint)endPoint
{
	self.gradientLayer.endPoint = endPoint;
}

-(CAGradientLayer *)gradientLayer
{
	return (CAGradientLayer *)self.layer;
}
@end

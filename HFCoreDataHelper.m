//
//  CoreDataHelper.h
//  GroceryList
//
//  Created by Keith Alperin on 4/30/10.
//  Copyright 2010 Helium Foot Software. All rights reserved.
//

/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
	(i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
	(ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
	(iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/



#import "HFCoreDataHelper.h"

@interface HFCoreDataHelper ()
- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinatorWithName:(NSString *)name error:(NSError **)error;
- (NSString *)persistentStorePathForName:(NSString *)name;
@end


@implementation HFCoreDataHelper

#pragma mark -
#pragma mark Core Data stack

/**
 * Returns the managed object context for the application.
 * If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 * will return nil if we can't create the persistent store
 */
- (NSManagedObjectContext *) buildManagedObjectContextForName:(NSString *)name
{
	
    NSManagedObjectContext *aManagedObjectContext = nil;
	NSError *error = nil;
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinatorWithName:name error:&error];
    if (coordinator && [coordinator.persistentStores count]) {
        aManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
		
        [aManagedObjectContext setPersistentStoreCoordinator: coordinator];
    } else {
		NSLog(@"There is no persistentStoreCoordinator for %@!!", name);
	}
    return aManagedObjectContext;
}

- (BOOL)isExistingDataStoreWithNameCompatible:(NSString *)name
{
	NSURL *storeUrl = [NSURL fileURLWithPath:[self persistentStorePathForName:name]];
	NSError *error = nil;
	NSDictionary *metadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeUrl error:&error];
	BOOL isCompatible = [[self managedObjectModel] isConfiguration:nil compatibleWithStoreMetadata:metadata];
	
	return isCompatible;
	
	
}

- (BOOL)persistentStoreExistsForName:(NSString *)name
{
	NSString *path = [self persistentStorePathForName:name];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	BOOL exists = [fileManager fileExistsAtPath:path];
	
	return exists;
}

- (BOOL)removePersistentStoreName:(NSString *)name
{
	NSString *path = [self persistentStorePathForName:name];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSError *error = nil;
	BOOL removed = [fileManager removeItemAtPath:path error:&error];
	if (error != nil) {
		NSLog(@"Unresolved error removing persistent store at %@: %@, %@",path, error, [error userInfo]);
	}
	
	return removed;
	
}

- (BOOL)replacePersistentStoreWithName:(NSString *)name withFileAtPath:(NSString *)path
{
	[self removePersistentStoreName:name];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSError *error = nil;
	NSString *persistentStoreDestinationPath = [self persistentStorePathForName:name];
	NSString *persistentStoreDestinationDirectory = [persistentStoreDestinationPath stringByDeletingLastPathComponent];
	[fileManager createDirectoryAtPath:persistentStoreDestinationDirectory
		   withIntermediateDirectories:YES
							attributes:nil error:&error];
	if (error != nil) {
		NSLog(@"Unresolved error creating directory at %@: %@, %@",persistentStoreDestinationDirectory, error, [error userInfo]);
	}
	
	error = nil;
	BOOL moved = [fileManager copyItemAtPath:path toPath:persistentStoreDestinationPath error:&error];
	if (error != nil) {
		NSLog(@"Unresolved error moving persistent store at %@ to %@: %@, %@",path, persistentStoreDestinationPath ,error, [error userInfo]);
	}
	return moved;
	
}
-(void)close:(NSManagedObjectContext *)managedObjectContext
{
	NSError *error = nil;
	if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		
		
	}
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel
{
	
	NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return managedObjectModel;
}

/*
 * will return nil if we can't open the persistent store.  check the error
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinatorWithName:(NSString *)name error:(NSError **)error
{
	
	NSError *localError = nil;
	NSString *storePath = [self persistentStorePathForName:name];
	NSString *storeDirectoryPath = [storePath stringByDeletingLastPathComponent];
	[[NSFileManager defaultManager] createDirectoryAtPath:storeDirectoryPath withIntermediateDirectories:YES attributes:nil error:&localError];
    NSURL *storeUrl = [NSURL fileURLWithPath:[self persistentStorePathForName:name]];
	
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]] ;
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
							 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
							 nil];
	NSPersistentStore *persistentStore = [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&localError];
	NSDictionary *attributes = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
	NSError *protectionError = nil;
	BOOL success = [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:storePath error:&protectionError];
	if (!success && protectionError != nil) {
		NSLog(@"Unresolved error %@, %@", protectionError, [protectionError userInfo]);
	}
	
	
	if (!persistentStore) {
		NSLog(@"Unresolved error creating persistent store named %@: %@, %@", name, localError, [localError userInfo]);
		
		
		if (localError != nil) {
			NSError *underlyingError = [[localError userInfo] objectForKey:NSUnderlyingErrorKey];
			if (underlyingError) {
				NSLog(@"Underlying error: %@ \n\n userInfo: %@ \n\n", underlyingError, [underlyingError userInfo]);
				NSArray *detailedErrors = [[underlyingError userInfo] objectForKey:NSDetailedErrorsKey];
				for (NSError *detailError in detailedErrors) {
					NSLog(@"Detailed error: %@ \n\n userInfo: %@ \n\n", detailError, [detailError userInfo]);
				}
				
			}
			
			NSArray *detailedErrors = [[localError userInfo] objectForKey:NSDetailedErrorsKey];
			for (NSError *detailError in detailedErrors) {
				NSLog(@"Detailed error: %@ \n\n userInfo: %@ \n\n", detailError, [detailError userInfo]);
			}
		}
		coordinator = nil;
	}
	if (error != NULL && localError != nil) {
		*error = localError;
	}
	
	
    return coordinator;
}

- (NSString *)persistentStorePathForName:(NSString *)name
{
    NSString *storageDirectoryPath = nil;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *applicationSupportPath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
	if (applicationSupportPath != nil) {
		storageDirectoryPath = [applicationSupportPath stringByAppendingPathComponent:name];
	}
	
	return [[storageDirectoryPath stringByAppendingPathComponent:name] stringByAppendingPathExtension:@"sqlite"];
}


@end

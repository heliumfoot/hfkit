//
//  UIView+HFExtensions.m
//	HFKit
//
//  Created by Keith Alperin
//  Copyright (c) 2012 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/


#import "UIView+HFExtensions.h"

@implementation UIView (hfExtensions)
+(UIViewAnimationOptions)hfAnimationOptionCurveForUIViewAnimationCurve:(UIViewAnimationCurve)curve
{
	
	UIViewAnimationOptions option = UIViewAnimationOptionCurveLinear;
	switch (curve) {
        case UIViewAnimationCurveEaseInOut:
            option = UIViewAnimationOptionCurveEaseInOut;
        case UIViewAnimationCurveEaseIn:
            option = UIViewAnimationOptionCurveEaseIn;
        case UIViewAnimationCurveEaseOut:
            option = UIViewAnimationOptionCurveEaseOut;
        case UIViewAnimationCurveLinear:
            option = UIViewAnimationOptionCurveLinear;
    }
	
	return option;
}


-(CGSize)hfSize
{
	return self.bounds.size;
}
-(void)setHfSize:(CGSize)size
{
	CGRect bounds = self.bounds;
	bounds.size = size;
	self.bounds = bounds;
}
-(CGFloat)hfSizeWidth
{
	return self.hfSize.width;
}
-(CGFloat)hfSizeHeight
{
	return self.hfSize.height;
}
-(void)setHfSizeWidth:(CGFloat)width
{
	CGRect bounds = self.bounds;
	bounds.size.width = width;
	self.bounds = bounds;
}
-(void)setHfSizeHeight:(CGFloat)height
{
	CGRect bounds = self.bounds;
	bounds.size.height = height;
	self.bounds = bounds;
}

-(CGPoint)hfOrigin
{
	return self.frame.origin;
}
-(CGFloat)hfOriginX
{
	return self.hfOrigin.x;
}
-(CGFloat)hfOriginY
{
	return self.hfOrigin.y;
}
-(void)setHfOrigin:(CGPoint)origin
{
	CGRect frame = self.frame;
	frame.origin = origin;
	self.frame = frame;
}
-(void)setHfOriginX:(CGFloat)originX
{
	CGRect frame = self.frame;
	frame.origin.x = originX;
	self.frame = frame;
}
-(void)setHfOriginY:(CGFloat)originY
{
	CGRect frame = self.frame;
	frame.origin.y = originY;
	self.frame = frame;
}

-(void)hfCenterInSuperview
{
	[self hfCenterHorizontallyInSuperview];
	[self hfCenterVerticallyInSuperview];
}
-(void)hfCenterHorizontallyInSuperview
{
	self.hfOriginX = round((self.superview.hfSize.width - self.hfSize.width) / 2.0);
}

-(void)hfCenterVerticallyInSuperview
{
	self.hfOriginY = round((self.superview.hfSize.height - self.hfSize.height) / 2.0);
}

-(id)hfSuperiewOfType:(Class)type
{
	UIView *superView = [self superview];
	UIView *superViewOfCorrectType = nil;
	if ([superView isKindOfClass:type] || superView == nil) {
		superViewOfCorrectType = superView;
	} else {
		superViewOfCorrectType = [superView hfSuperiewOfType:type];
	}
	return superViewOfCorrectType;
	
}

- (id)hfFirstResonder
{
    if (self.isFirstResponder) {        
        return self;     
    }
	
    for (UIView *subView in self.subviews) {
        UIView *firstResponder = [subView hfFirstResonder];
		
        if (firstResponder != nil) {
			return firstResponder;
        }
    }
	
    return nil;

}
@end

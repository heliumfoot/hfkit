//
//  ManagedObjectContextHelper.m
//	HFKit
//
//  Created by keith Alperin on 7/23/13.
//  Copyright (c) 2013 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "HFManagedObjectContextHelper.h"

@interface HFManagedObjectContextHelper ()
@property(nonatomic,strong)NSManagedObjectContext* foregroundManagedObjectContext;

@end

@implementation HFManagedObjectContextHelper
- (id)initWithForegroundManagedObjectContext:(NSManagedObjectContext *)foregroundManagedObjectContext
{
    self = [super init];
    if (self) {
        self.foregroundManagedObjectContext = foregroundManagedObjectContext;
    }
    return self;
}
-(void)updateWithForegroundManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	self.foregroundManagedObjectContext = managedObjectContext;
}
-(id)managedObject:(NSManagedObject*)managedObject inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
{
	id objectInContext = [managedObjectContext objectWithID:managedObject.objectID];
	return objectInContext;
}

-(NSManagedObjectContext*)temporaryBackgroundManagedObjectContext
{
	NSManagedObjectContext* temporaryBackgroundManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	temporaryBackgroundManagedObjectContext.parentContext = self.foregroundManagedObjectContext;
	
	return temporaryBackgroundManagedObjectContext;
}

-(void)saveWithManagedObjectContext:(NSManagedObjectContext*)managedObjectContext successHandler:(void (^)())successHandler failureHandler:(void (^)(NSManagedObjectContext* managedObjectContext, NSError*))failureHandler
{

	
	void(^saveBlock)() = ^() {
		NSError *error = nil;
		BOOL saved = [managedObjectContext save:&error];
		if (!saved) {
			if (error != nil) {
				NSLog(@"Unresolved error saving %@, %@", error, [error userInfo]);
			}
			failureHandler(managedObjectContext, error);
		} else {
			if (managedObjectContext.parentContext != nil) {
				[self saveWithManagedObjectContext: managedObjectContext.parentContext successHandler:successHandler failureHandler:failureHandler];
			} else {
				successHandler();
			}
			
		}
	};
	
	BOOL isMainContext = managedObjectContext.parentContext == nil;
	
	if (isMainContext) {
		[managedObjectContext performBlockAndWait:saveBlock];
	} else {
		[managedObjectContext performBlock:saveBlock];
	}
	
}

-(NSManagedObjectContext *)currentManagedObjectContext
{
	return self.foregroundManagedObjectContext;
}
@end

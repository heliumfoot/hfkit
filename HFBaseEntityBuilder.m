//
//  HFBaseEntityBuilder.m
//	HFKit
//
//  Created by keith Alperin on 7/26/13.
//  Copyright (c) 2013 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "HFBaseEntityBuilder.h"

@interface HFBaseEntityBuilder ()
@property(nonatomic,strong)NSManagedObjectContext* backgroundManagedObjectContext;
@property(nonatomic,strong)NSString* entityIDExternalKeyName;
@property(nonatomic,strong)NSString* entityIDInternalKeyName;
-(id)buildOrRetreiveEntityOfType:(Class)type withResultsDictionary:(NSDictionary*)resultDictionary;
-(id)buildUnsavedEntityOfType:(Class)type forID:(NSInteger)entityID;
-(id)entityForID:(NSInteger)entityID ofType:(Class)type inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext;
-(NSDictionary*)dictionaryByRemovingNullsFromDictionary:(NSDictionary*)dictionary;
@end

@implementation HFBaseEntityBuilder
-(id)initWithBackgroundManagedObjectContext:(NSManagedObjectContext*)backgroundManagedObjectContext entityIDExternalKeyName:(NSString*)entityIDExternalKeyName entityIDInternalKeyName:(NSString*)entityIDInternalKeyName
{
    self = [super init];
    if (self) {
        self.backgroundManagedObjectContext = backgroundManagedObjectContext;
		self.entityIDExternalKeyName = entityIDExternalKeyName;
		self.entityIDInternalKeyName = entityIDInternalKeyName;
    }
    return self;
}

-(Class)entityType
{
	return nil;
}


-(id)buildOrUpdateEntityWithResultsDictionary:(NSDictionary*)resultDictionary
{
	NSDictionary* filteredDictionary = [self dictionaryByRemovingNullsFromDictionary:resultDictionary];
	id entity = [self buildOrRetreiveEntityOfType:[self entityType] withResultsDictionary:filteredDictionary];
	[self updateEntity:(id)entity withResultDictionary:(NSDictionary*)resultDictionary];
	
	return entity;
}
-(void)updateEntity:(id)entity withResultDictionary:(NSDictionary*)dictionary
{
	NSAssert(NO, @"Overridden by subclasses");
	
}

-(id)buildOrRetreiveEntityOfType:(Class)type withResultsDictionary:(NSDictionary*)resultDictionary
{
	NSManagedObject* entity = nil;
	NSInteger entityID = [self entityIDForResultDictionary:resultDictionary];
	if (entityID != NSNotFound) {
		entity = [self entityForID:entityID ofType:type inManagedObjectContext:self.backgroundManagedObjectContext];
		if (entity == nil) {
			entity = [self buildUnsavedEntityOfType:type forID:entityID];
		}
	}
	return entity;
}

-(id)buildUnsavedEntityOfType:(Class)type forID:(NSInteger)entityID
{
	NSManagedObject* entity = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([type class]) inManagedObjectContext:self.backgroundManagedObjectContext];
	[entity setValue:@(entityID) forKey:self.entityIDInternalKeyName];
	
	return entity;
	
}

-(id)entityForID:(NSInteger)entityID ofType:(Class)type inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSString *entityName = NSStringFromClass(type);
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	fetchRequest.entity = entity;
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %d", self.entityIDInternalKeyName, entityID];
	fetchRequest.predicate = predicate;
	
	NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"updated" ascending:YES];
	fetchRequest.sortDescriptors = @[sortDescriptor];
	
	NSError *error = nil;
	NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
	if (fetchedObjects == nil) {
		NSLog(@"Error fetching %@: %@\n%@",entityName, error, [error userInfo]);
	}
	
	return [fetchedObjects lastObject];
	
}

-(NSInteger)entityIDForResultDictionary:(NSDictionary*)resultDictionary
{
	NSNumber* entityIDNumber = resultDictionary[self.entityIDExternalKeyName];
	NSInteger entityID = entityIDNumber == nil?NSNotFound:[entityIDNumber integerValue];
	return entityID;
	
}
#pragma mark Utility
-(NSDictionary*)dictionaryByRemovingNullsFromDictionary:(NSDictionary*)dictionary
{
	NSMutableDictionary* newDictionary = [NSMutableDictionary dictionaryWithCapacity:dictionary.count];
	
	[dictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		if (![obj isEqual:[NSNull null]]) {
			newDictionary[key] = obj;
		}
	}];
	
	return [NSDictionary dictionaryWithDictionary:newDictionary];
	
}


-(NSDate*)dateFromDictionary:(NSDictionary*)dictionary forKey:(NSString*)key
{
	NSDate* date = nil;
	NSNumber* timestampAsNumber = dictionary[key];
	if (timestampAsNumber != nil) {
		date = [NSDate dateWithTimeIntervalSinceReferenceDate:[timestampAsNumber doubleValue]];
	}
	return date;
	
}
@end

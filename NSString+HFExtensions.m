//
//  NSString+HFExtensions.m
//	HFKit
//
//  Created by Keith Alperin on 4/30/10.
//  Copyright 2010 Helium Foot Software. All rights reserved.
//
/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import "NSString+HFExtensions.h"

@implementation NSString (HFExtensions)
//with thanks to Dima: http://stackoverflow.com/a/10556156/16572
+(NSString *)hfUniqueString
{
	// Create universally unique identifier (object)
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
	
    // Get the string representation of CFUUID object.
    NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidObject);
    CFRelease(uuidObject);
	
	return uuidStr;
}
+(NSString *)hfStringWithUTF8EncodedData:(NSData *)data
{
	NSString *string = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
	return string;
}
-(BOOL)hfPresent
{
	//will be NO if called on nil
	return [self length] > 0;
}

-(NSString *)hfTrimmedString
{
	return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSString *)hfStringByNormalizingDiacritics
{
	NSMutableString *mString = [[self decomposedStringWithCanonicalMapping] mutableCopy];
	NSCharacterSet *nonBaseSet = [NSCharacterSet nonBaseCharacterSet];
	NSRange range = NSMakeRange([mString length], 0);
	
	while (range.location > 0) {
		range = [mString rangeOfCharacterFromSet:nonBaseSet options:NSBackwardsSearch range:NSMakeRange(0, range.location)];
		if (range.length == 0) break;
		[mString deleteCharactersInRange:range];
	}
	
	NSString *returnedString = [NSString stringWithString:mString];
	return returnedString;
}

-(NSString *)hfStringByNormalizingForSorting
{
	NSString *normalizedString = [[[self lowercaseString] hfStringByNormalizingDiacritics] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	return normalizedString;
}

-(NSString *)hfSubstringUpToIndex:(NSUInteger)anIndex
{
	return [self substringToIndex:(anIndex > [self length])?[self length]:anIndex];
}

@end

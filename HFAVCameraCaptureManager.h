//
//  HFAVCameraCaptureManager.h
//	HFKit
//
//  Created by Keith Alperin on 04/02/12.
//  Copyright (c) 2012 Helium Foot Software. All rights reserved.
//
//	Based heavily on Apple's AVCamCaptureManager.h, the comments on which are below.
//
/*
     File: AVCamCaptureManager.h
 Abstract: Code that calls the AVCapture classes to implement the camera-specific features in the app such as recording, still image, camera exposure, white balance and so on.
  Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 */

/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
 (i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
 (ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
 (iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/


#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef enum {
    HFCameraMirroringOff   = 1,
    HFCameraMirroringOn    = 2,
    HFCameraMirroringAuto  = 3
} HFCameraMirroringMode;

typedef enum {
    HFCameraCaptureModeNone    = 0,
    HFCameraCaptureModeStill   = 1,
    HFCameraCaptureModeVideo   = 2
} HFCameraCaptureMode;

@protocol HFCameraCoreImageFrameCapturingDelegate <NSObject>
- (void) captureFrameSucceededWithImage:(CIImage *)image options:(NSDictionary *)options;

@end
@protocol HFCameraUIImageFrameCapturingDelegate <NSObject>
- (void) captureFrameSucceededWithUIImage:(UIImage *)image options:(NSDictionary *)options;

@end

@protocol HFCameraCaptureManagerDelegate <NSObject>
@optional
- (void) captureStillImageSucceededWithImage:(UIImage *)image metadata:(NSDictionary *)metadata;
- (void) captureStillImageFailedWithError:(NSError *)error;
- (void) acquiringDeviceLockFailedWithError:(NSError *)error;
- (void) someOtherError:(NSError *)error;
- (void) recordingBeganToOutputFileAtURL:(NSURL *)url;
- (void) recordingFinishedToOutputFileAtURL:(NSURL *)url;
- (void) deviceCountChanged;
//- (void) adjustedFocus;
@end

extern NSString * const kNotifcationFocusAdjusted;


@interface HFAVCameraCaptureManager : NSObject {
}

@property (nonatomic,readonly,strong) AVCaptureSession *session;
@property (nonatomic,assign) AVCaptureVideoOrientation orientation;
@property (nonatomic,readonly,strong) AVCaptureAudioChannel *audioChannel;
@property (nonatomic,strong) NSString *sessionPreset;
@property (nonatomic,assign) HFCameraMirroringMode mirroringMode;
@property (nonatomic,readonly,strong) AVCaptureDeviceInput *videoInput;
@property (nonatomic,readonly,strong) AVCaptureDeviceInput *audioInput;
@property (nonatomic,assign) AVCaptureFlashMode flashMode;
@property (nonatomic,assign, readonly) NSArray *flashModes;
@property (nonatomic,assign) AVCaptureTorchMode torchMode;
@property (nonatomic,assign) AVCaptureFocusMode focusMode;
@property (nonatomic,assign) AVCaptureExposureMode exposureMode;
@property (nonatomic,assign) AVCaptureWhiteBalanceMode whiteBalanceMode;
@property (nonatomic,readonly,strong) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic, weak) id <HFCameraCaptureManagerDelegate> delegate;
@property (nonatomic,readonly,getter=isRecording) BOOL recording;
@property (nonatomic,readonly,getter=isSessionRunning) BOOL sessionRunning;
@property(nonatomic, assign)HFCameraCaptureMode captureMode;
@property(nonatomic, strong, readonly)AVCaptureDevice * currentCameraDevice;

//- (BOOL) setupSessionWithPreset:(NSString *)sessionPreset preferredCamera:(AVCaptureDevicePosition)preferredCamera error:(NSError **)error;
- (BOOL) setupSessionWithMode:(HFCameraCaptureMode)captureMode preset:(NSString *)sessionPreset preferredCamera:(AVCaptureDevicePosition)preferredCamera error:(NSError **)error;
- (void) stopSession;
- (void) startSession;
- (void) startRecording;
- (void) stopRecording;
- (void) cancelRecording;
- (void) captureStillImage;
- (void) captureNextVideoCoreImageFrameWithDelegate:(id<HFCameraCoreImageFrameCapturingDelegate>)aFrameCapturingDelegate;
- (void) captureNextVideoUIImageFrameWithDelegate:(id<HFCameraUIImageFrameCapturingDelegate>)aFrameCapturingDelegate;
- (BOOL) cameraToggle;
- (NSUInteger) cameraCount;
- (NSUInteger) micCount;
- (BOOL) hasFlash;
- (BOOL) hasTorch;
-(void)flashNumberOfTimes:(NSInteger)count;
- (BOOL) hasFocus;
- (BOOL) hasExposure;
- (BOOL) hasWhiteBalance;
- (void) focusAtPoint:(CGPoint)point;
- (void) exposureAtPoint:(CGPoint)point;
- (void) setConnectionWithMediaType:(NSString *)mediaType enabled:(BOOL)enabled;
- (BOOL) supportsMirroring;
+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections;

@end

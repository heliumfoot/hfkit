//
//  VanillaFetchedResultsControllerDelegate.h
//
//  Created by keith Alperin on 7/25/13.
//  Copyright (c) 2013 Helium Foot Software. All rights reserved.
//

/***
 With respect to the following source code included in the custom software developed by Helium Foot Software, Inc.,
	(i) such source code shall remain the exclusive property of Helium Foot Software, Inc. and may be utilized in other work and for other clients,
	(ii) Helium Foot Software, Inc. grants ￼<# client name #> (“Client”) an irrevocable, non-exclusive, transferable, worldwide, perpetual, royalty-free right and license to use such source code in the custom software developed by Helium Foot Software, Inc. and
	(iii) use of such source code separate and apart from any services contracted with Helium Foot Software, Inc. is without warranty of any kind.  Client’s use of the custom software containing such source code shall be deemed to constitute Client’s agreement to these terms with respect to which Client acknowledges having notice of notwithstanding the absence of any Client signature hereto.
 ***/

#import <Foundation/Foundation.h>

@interface HFVanillaFetchedResultsControllerDelegate : NSObject<NSFetchedResultsControllerDelegate>
@property (nonatomic, readonly) BOOL ignoringUpdates;
@property (nonatomic, copy) void (^tableAnimationCompletionBlock)(NSSet* insertedIndexPaths);

- (id)initWithTableView:(UITableView*)tableView insertCellBlock:(void (^)(NSIndexPath* indexPath))insertCellBlock deleteCellBlock:(void (^)(NSIndexPath* indexPath))deleteCellBlock;
- (id)initWithTableView:(UITableView*)tableView insertCellBlock:(void (^)(NSIndexPath* indexPath))insertCellBlock deleteCellBlock:(void (^)(NSIndexPath* indexPath))deleteCellBlock moveCellBlock:(void (^)(NSIndexPath* deletedRowIndexPath,NSIndexPath* insertedRowIndexPath))moveCellBlock endUpdatesBlock:(void (^)())endUpdatesBlock;
-(void)ignoreUpdates;
-(void)resumeUpdates;

@end
